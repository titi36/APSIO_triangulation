#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <string>
#include <iostream>

cv::Mat imgOriginal;  // Image d'entrée
cv::Mat threshImg;   // Image Tresh
cv::Mat hsvImg;    // Image HSV

 // Vector d'objet Vec3f, utilisé par la méthode HoughCircles (qui recherche les cercles dans l'image)
 std::vector<cv::Vec3f> v3fCircles;  
 std::string imagesPath[6] = { "Gauche_gauche.jpg","Gauche_milieu.jpg","Gauche_droite.jpg",
						"Droite_gauche.jpg","Droite_milieu.jpg","Droite_droite.jpg"};
				 
 //std::string imagesPath[6] = { "Droite_gauche.jpg","Droite_gauche.jpg","Droite_gauche.jpg",
	//						"Droite_gauche.jpg","Droite_gauche.jpg","Droite_gauche.jpg"};		  
 int lowH = 79;       // Hue
 int highH = 106;
 int lowS = 55;       // Saturation
 int highS = 255;
 int lowV = 38;       // Value
 int highV = 210;

void run_image_processing( int, void* )
{  
// Transforme l'imge en TreshImage
  cv::cvtColor(imgOriginal, hsvImg, CV_BGR2HSV);

  cv::inRange(hsvImg, cv::Scalar(lowH, lowS, lowV), cv::Scalar(highH, highS, highV), threshImg);

// Traitements divers
  cv::GaussianBlur(threshImg, threshImg, cv::Size(3, 3), 0);   
  cv::dilate(threshImg, threshImg, 0);        
  cv::erode(threshImg, threshImg, 0);         
  
  // hough detector works better with some smoothing of the image
  cv::GaussianBlur( threshImg, threshImg, cv::Size(1,1), 9, 9 );
		
  // Rempli les vercteurs de cercles avec la fonction opencv
  cv::HoughCircles(threshImg,v3fCircles,CV_HOUGH_GRADIENT,2,threshImg.rows / 4,150,50,200,1500);
  
  // Met à jour les popup
  cv::imshow("imgOriginal", imgOriginal);    
  cv::imshow("threshImg", threshImg);
}


int main() {

// Parcours des 6 images
int i = 1;
for(const std::string &path : imagesPath){   
 
	 std::cout << "====== Image N° " << i << " fichier : " << path << "======\n\n";
	 i++;
 
	  // Récupératin de l'image
	  imgOriginal = cv::imread(path,1);
	  
	  // Si erreur pendant la lecture
	  if (imgOriginal.empty()) { 
	   std::cout << "error: frame can't read \n";     
	   break;              
	  }

// Transforme l'imge en TreshImage
  cv::cvtColor(imgOriginal, hsvImg, CV_BGR2HSV);

  cv::inRange(hsvImg, cv::Scalar(lowH, lowS, lowV), cv::Scalar(highH, highS, highV), threshImg);

// Traitements divers
  cv::GaussianBlur(threshImg, threshImg, cv::Size(3, 3), 0);   
  cv::dilate(threshImg, threshImg, 0);        
  cv::erode(threshImg, threshImg, 0);         
  
  // hough detector works better with some smoothing of the image
  cv::GaussianBlur( threshImg, threshImg, cv::Size(1,1), 9, 9 );
		
  // Rempli les vercteurs de cercles avec la fonction opencv
  cv::HoughCircles(threshImg,v3fCircles,CV_HOUGH_GRADIENT,2,threshImg.rows / 4,100,50,10,1500);

	std::cout << "Nombre de cercles = " << v3fCircles.size() << "\n";     

// Pour chaque cercle trouvé
  for (int i = 0; i < v3fCircles.size(); i++) {
           
    
   std::cout << "Ball position X = "<< v3fCircles[i][0] // Position x du centre du cercle
    <<",\tY = "<< v3fCircles[i][1]        				// Position y du centre du cercle
    <<",\tRadius = "<< v3fCircles[i][2]<< "\n";		// Rayon du cercle

	// Dessine un cercle vert au centre du cercle trouvé
   cv::circle(imgOriginal,            
    cv::Point((int)v3fCircles[i][0], (int)v3fCircles[i][1]),  // Point centre du cercle
    3,                										  // Rayon du petit cercle
    cv::Scalar(0, 255, 0),           						  // Couleur verte
    CV_FILLED);              								  // Epaisseur
	
	// Dessine un cercle rouge représentant le cercle 
   cv::circle(imgOriginal,            
    cv::Point((int)v3fCircles[i][0], (int)v3fCircles[i][1]),  // Centre du cercle
    (int)v3fCircles[i][2],           						  // Rayon du cercle
    cv::Scalar(0, 0, 255),                                    // Couleur rouge
    3);

	std::cout << "\n";                										 
  } 

	// Ouvre les fenêtres
	cv::namedWindow("imgOriginal", 500);
	cv::namedWindow("threshImg", 500); 
	cv::namedWindow("trackbars", 500); 

	// Affiche les images
	cv::imshow("imgOriginal", imgOriginal);    
	cv::imshow("threshImg", threshImg);
  
	// Création des trackbars pour modifier les valeurs de détection
	cv::createTrackbar("LowH", "trackbars", &lowH, 179, run_image_processing); //Hue (0 - 179)
	cv::createTrackbar("HighH", "trackbars", &highH, 179, run_image_processing);

	cv::createTrackbar("LowS", "trackbars", &lowS, 255, run_image_processing); //Saturation (0 - 255)
	cv::createTrackbar("HighS", "trackbars", &highS, 255, run_image_processing);

	cv::createTrackbar("LowV", "trackbars", &lowV, 255,run_image_processing); //Value (0 - 255)
	cv::createTrackbar("HighV", "trackbars", &highV, 255, run_image_processing);
  
	 // Attends
	 cv::waitKey(0);
 }
 
 return(0);
           
}
